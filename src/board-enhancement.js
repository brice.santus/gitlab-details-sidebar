const CLASSES = {
  CARD: 'board-card',
  CARD_ACTIVE: 'is-active',
  CARD_GHOST: 'is-ghost',
  IFRAME: 'sidebar-iframe',
  IFRAME_LOADING: 'sidebar-iframe--loading',
  SIDEBAR_COLLAPSED: 'gitlab-sidebar-detail--collapsed',
  SPINNER: 'gitlab-sidebar-loading-spinner',
  TOGGLE: 'gitlab-sidebar-expanded-toggle',
  TOGGLE_COLLAPSED: 'gitlab-sidebar-expanded-toggle--collapsed',
}

const SELECTORS = {
  BOARD: '.boards-list',
  CARD: `.${CLASSES.CARD}`,
  IFRAME: `.${CLASSES.IFRAME}`,
  LINK: '.board-card-title a',
  NUMBER: '.board-card-number',
  SIDEBAR: '.issue-boards-sidebar',
  SIDEBAR_HEADER: '.issuable-sidebar-header',
}

const LOCAL_STORAGE_KEY = 'gitlab-sidebar-expanded-state';

const debounce = (f, t) => {
  let timeout;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => f(...args), t);
  };
};

class BoardEnhancement {
  constructor(element) {
    if (!element) {
      return;
    }

    this.element = element;
    this.sidebar = document.querySelector(SELECTORS.SIDEBAR);
    this.sidebarHeader = this.sidebar.querySelector(SELECTORS.SIDEBAR_HEADER);
    this.frame = null;
    this.toggle = null;
    this.cardList = {};
    this.firstRun = true;

    this.clickHandler = this.clickHandler.bind(this);
    this.getCardLink = this.getCardLink.bind(this);
    this.getCardNumber = this.getCardNumber.bind(this);
    this.onBoardMutation = this.onBoardMutation.bind(this);
    this.setIssue = this.setIssue.bind(this);
    this.setUIState = this.setUIState.bind(this);
    this.styleFrame = this.styleFrame.bind(this);
    this.toggleExpansion = this.toggleExpansion.bind(this);

    this.setIssueDebounced = debounce(this.setIssue, 1);
    this.expanded = localStorage.getItem(LOCAL_STORAGE_KEY) !== 'false';
    this.observer = new MutationObserver(this.onBoardMutation);

    this.bindEvents();
    console.debug('GitLab Details Sidebar Plugin initialised...');
  }

  bindEvents() {
    this.element.addEventListener('click', this.clickHandler);
    this.observer.observe(this.element, {
      attributes: false,
      childList: true,
      subtree: true,
    });
  }

  unbindEvents() {
    this.element.removeEventListener('click', this.clickHandler);
    this.observer.disconnect();
  }

  clickHandler(event) {
    const card = event.target.closest(SELECTORS.CARD);
    if (!card || event.target.closest(SELECTORS.LINK)) {
      return;
    }

   const link = this.getCardLink(card);
   if (link) {
     this.setIssue(link);
   }
  }

  getCardLink(card) {
    const link = card.querySelector(SELECTORS.LINK);
    if (!link) {
      console.warn('No link found on the current card.');
      return;
    }
    return link.href;
  }

  getCardNumber(card) {
    const numberEl = card.querySelector(SELECTORS.NUMBER);
    if (!numberEl) {
      console.warn('No number found on the current card.');
      return;
    }
    return numberEl.textContent.trim().replace('#', '');
  }

  setIssue(url) {
    if (this.firstRun) {
      this.firstRun = false;
      return;
    }
    console.log(`Setting iframe URL to: ${url}`);
    if (!this.frame) {
      this.createFrame();
    }
    this.frame.classList.add(CLASSES.IFRAME_LOADING);
    this.frame.src = url;
  }

  createFrame() {
    this.frame = document.createElement('iframe');

    if (document.querySelector(SELECTORS.IFRAME)) {
      // We have somehow double initialised, don't create a second frame, die gracefully
      console.warn('GitLab Details Sidebar appears to have been initialised twice');
      this.destroy();
      return;
    }

    this.toggle = document.createElement('button');
    this.frame.addEventListener('load', this.styleFrame);
    this.toggle.addEventListener('click', this.toggleExpansion);
    
    const div = document.createElement('div');
    const spinner = document.createElement('span');
    const sidebarContent = this.sidebar.firstElementChild;
    
    div.appendChild(spinner);
    div.appendChild(this.toggle);
    div.appendChild(this.frame);
    div.className = CLASSES.IFRAME;
    spinner.className = CLASSES.SPINNER;
    this.toggle.className = CLASSES.TOGGLE;
    this.toggle.ariaLabel = 'Toggle Sidebar Details View';
    this.toggle.title = 'Toggle Sidebar Details View';

    this.sidebar.insertBefore(div, sidebarContent);
    this.sidebarHeader.appendChild(this.toggle);
    this.setUIState();
  }

  styleFrame() {
    const style = document.createElement('style');
    style.textContent = `
      .navbar,
      .alert-wrapper,
      .right-sidebar,
      .nav-sidebar,
      .issuable-gutter-toggle {
        display: none !important;
      }

      .content-wrapper {
        margin-top: 0 !important;
        padding-right: 0 !important;
      }

      .layout-page.page-gutter {
        padding-left: 0 !important;
      }
    `;
    const iframeDefaultTarget = '_top';
    const base = document.createElement('base');
    base.setAttribute('target', iframeDefaultTarget);

    try {
      const iframeDocument = this.frame.contentWindow.document;
      iframeDocument.head.appendChild(style);
      iframeDocument.head.appendChild(base);
      [...iframeDocument.querySelectorAll('a')]
        .filter(a => a.target === '_self')
        .forEach(a => a.target = iframeDefaultTarget);
    } catch (err) {
      console.warn('GitLab Details Sidebar was unable to style the child frame.');
      console.debug(err);
    }

    this.frame.classList.remove(CLASSES.IFRAME_LOADING);
  }

  toggleExpansion() {
    this.expanded = !this.expanded;
    localStorage.setItem(LOCAL_STORAGE_KEY, `${this.expanded}`);
    this.setUIState();
  }
  
  setUIState() {
    if (this.expanded) {
      this.toggle.classList.remove(CLASSES.TOGGLE_COLLAPSED);
      document.body.classList.remove(CLASSES.SIDEBAR_COLLAPSED);
    } else {
      this.toggle.classList.add(CLASSES.TOGGLE_COLLAPSED);
      document.body.classList.add(CLASSES.SIDEBAR_COLLAPSED);
    }
  }

  onBoardMutation(mutationList) {
    mutationList
      .filter((mutation) => {
        if (mutation.type !== 'childList' || mutation.addedNodes.length !== 1) {
          return false;
        }
        const newEl = mutation.addedNodes[0]
        return newEl.classList &&
          newEl.classList.contains(CLASSES.CARD) &&
          !newEl.classList.contains(CLASSES.CARD_GHOST) &&
          newEl.getAttribute('draggable') !== 'false';
      })
      .forEach((mutation) => {
        const card = mutation.addedNodes[0];
        const number = this.getCardNumber(card);
        if (this.cardList[number]) {
          // Existing card, just a drag and drop event
          return;
        }
        this.cardList[number] = true;
        const highestNumber = Math.max(...Object.keys(this.cardList));
        if (number < highestNumber) {
          // New tickets will always have the highest number, this must be the loading in of an existing ticket
          return;
        }
        const link = this.getCardLink(card);
        if (!link) {
          return;
        }
        // When the board is loaded, suddenly there are loads of new cards added
        this.setIssueDebounced(link);
      });
  }

  destroy() {
    this.unbindEvents();
    this.element = null;
    this.sidebar = null;
    this.sidebarHeader = null;
  }
}

const board = document.querySelector(SELECTORS.BOARD);
// eslint-disable-next-line no-unused-vars
const boardEnhancement = new BoardEnhancement(board);
